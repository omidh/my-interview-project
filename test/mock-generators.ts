import { Payment, PaymentStatus, User, UserRole } from '@prisma/client';
import { generate } from 'randomstring';

export function userMockGenerator(
  params: {
    username?: string;
    role?: UserRole;
  } = {},
) {
  params.username ??= generate({ length: 15, charset: 'alphanumeric' });
  params.role ??= 'SIMPLE';
  return {
    username: params.username,
    role: params.role,
  } as User;
}

export function paymentMockGenerator(
  params: {
    id?: string;
    username?: string;
    code?: number;
    planId?: number;
    status?: PaymentStatus;
    url?: string;
  } = {},
) {
  params.username ??= generate({ length: 15, charset: 'alphanumeric' });
  params.id ??= `cl${generate({ length: 23, charset: 'alphanumeric' })}`;
  params.status ??= 'SUCCESS';
  params.url ??= 'http://www.example.com/';
  params.code ??= Math.floor(Math.random() * (9999 - 1000) + 1000);
  params.planId ??= Math.floor(Math.random() * (999999 - 100000) + 100000);
  return {
    id: params.id,
    code: params.code,
    status: params.status,
    planId: params.planId,
    url: params.url,
    username: params.username,
  } as Payment;
}
