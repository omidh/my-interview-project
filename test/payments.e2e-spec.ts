import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { setupUser } from './setups/user-setup';
import { userMockGenerator } from './mock-generators';
import { User } from '@prisma/client';
import { JwtPayload } from '../src/authentication/types/jwt-payload.type';
import { CreatePaymentDto } from '../src/payments/dto/create-payment.dto';
import { FindPaymentsDto } from '../src/payments/dto/find-payments.dto';
import { PrismaService } from '../src/prisma/prisma.service';

describe('PaymentsController (e2e)', () => {
  let app: INestApplication;
  let user: User;
  let authToken: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  beforeEach(async () => {
    user = await setupUser(app, userMockGenerator());
    const jwtService = app.get<JwtService>(JwtService);
    const payload: JwtPayload = { username: user.username };
    authToken = jwtService.sign(payload, { secret: 'someSecret' });
  });

  it('/payments (POST)', async () => {
    const body: CreatePaymentDto = {
      planId: 123,
    };
    const response = await request(app.getHttpServer())
      .post('/payments')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authToken}`)
      .send(body)
      .expect(HttpStatus.CREATED);

    expect(response.body).toEqual(
      expect.objectContaining({ username: user.username, planId: 123 }),
    );
  });

  it('/payments (GET)', async () => {
    const body: FindPaymentsDto = {
      username: user.username,
    };
    const response = await request(app.getHttpServer())
      .get('/payments')
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${authToken}`)
      .query(body)
      .expect(HttpStatus.OK);

    const prismaService = app.get<PrismaService>(PrismaService);
    await expect(
      prismaService.payment.findMany({ where: { username: user.username } }),
    ).resolves.toEqual(expect.arrayContaining(response.body));
  });
});
