import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { AuthenticationController } from '../src/authentication/authentication.controller';
import { setupUser } from './setups/user-setup';
import { userMockGenerator } from './mock-generators';
import { LoginDto } from '../src/authentication/dto/login.dto';
import { GenerateVerificationCodeDto } from '../src/authentication/dto/generate-verification-code.dto';

describe('AuthenticationController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/authentication/login (POST)', async () => {
    const user = await setupUser(app, userMockGenerator());
    const body: LoginDto = {
      password: 'somePassword',
      username: user.username,
      verificationCode: '123456',
    };
    const response = await request(app.getHttpServer())
      .post('/authentication/login')
      .set('Content-Type', 'application/json')
      .send(body)
      .expect(HttpStatus.OK);

    const token = (
      response.body as Awaited<ReturnType<AuthenticationController['login']>>
    ).token;
    const jwtService = app.get<JwtService>(JwtService);
    expect(jwtService.verify(token, { secret: 'someSecret' })).toEqual(
      expect.objectContaining({ username: user.username }),
    );
  });

  it('/authentication/code (POST)', async () => {
    const user = await setupUser(app, userMockGenerator());
    const body: GenerateVerificationCodeDto = {
      password: 'somePassword',
      username: user.username,
    };
    const response = await request(app.getHttpServer())
      .post('/authentication/code')
      .set('Content-Type', 'application/json')
      .send(body)
      .expect(HttpStatus.OK);

    const code = (
      response.body as Awaited<
        ReturnType<AuthenticationController['generateVerificationCode']>
      >
    ).code;
    expect(code).toHaveLength(6);
  });
});
