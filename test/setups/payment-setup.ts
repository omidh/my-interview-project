import { INestApplication } from '@nestjs/common';
import { Payment } from '@prisma/client';
import { PrismaService } from '../../src/prisma/prisma.service';

export function setupPayment(
  app: INestApplication,
  generated: Payment,
): Promise<Payment> {
  const prismaService = app.get<PrismaService>(PrismaService);
  return prismaService.payment.create({
    data: {
      username: generated.username,
      code: generated.code,
      status: generated.status,
      url: generated.url,
      planId: generated.planId,
    },
  });
}
