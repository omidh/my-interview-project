import { INestApplication } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from '../../src/prisma/prisma.service';

export function setupUser(
  app: INestApplication,
  generated: User,
): Promise<User> {
  const prismaService = app.get<PrismaService>(PrismaService);
  return prismaService.user.create({
    data: { role: generated.role, username: generated.username },
  });
}
