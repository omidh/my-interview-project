-- CreateEnum
CREATE TYPE "UserRole" AS ENUM ('SIMPLE', 'MANAGER');

-- CreateEnum
CREATE TYPE "PaymentStatus" AS ENUM ('SUCCESS', 'FAIL');

-- CreateTable
CREATE TABLE "User" (
    "username" TEXT NOT NULL,
    "role" "UserRole" NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("username")
);

-- CreateTable
CREATE TABLE "Payment" (
    "id" TEXT NOT NULL,
    "username" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "code" SMALLINT NOT NULL,
    "planId" INTEGER NOT NULL,
    "status" "PaymentStatus" NOT NULL,

    CONSTRAINT "Payment_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Payment_username_idx" ON "Payment"("username");

-- AddForeignKey
ALTER TABLE "Payment" ADD CONSTRAINT "Payment_username_fkey" FOREIGN KEY ("username") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;
