import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient();

async function main() {
  await prisma.user.create({
    data: {
      role: 'MANAGER',
      username: 'manager'
    }
  });
  await prisma.user.create({
    data: {
      role: 'SIMPLE',
      username: 'customer'
    }
  });

}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  })