import { Injectable } from '@nestjs/common';

@Injectable()
export class VerifierService {
  /**
   * Just a dummy function that returns a random number string
   */
  async generateCode(userName: string): Promise<string> {
    return Math.floor(Math.random() * (999999 - 100000) + 100000).toString();
  }

  /**
   * Just a dummy function that always returns true
   */
  async verifyCode(userName: string, code: string): Promise<boolean> {
    return true;
  }
}
