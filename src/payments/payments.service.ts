import { Injectable } from '@nestjs/common';
import { Payment, Prisma } from '@prisma/client';
import { paymentMockGenerator } from '../../test/mock-generators';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class PaymentsService {
  private readonly repository: Prisma.PaymentDelegate<
    Prisma.RejectOnNotFound | Prisma.RejectPerOperation | undefined
  >;

  constructor(prismaService: PrismaService) {
    this.repository = prismaService.payment;
  }

  /**
   * create payment record with dummy data
   * @param {string} planId
   * @returns `Payment`
   */
  create(params: { planId: number; username: string }): Promise<Payment> {
    const generated = paymentMockGenerator({
      planId: params.planId,
      username: params.username,
    });
    return this.repository.create({ data: { ...generated, id: undefined } });
  }

  findMany(params: { where?: Prisma.UserWhereInput }): Promise<Payment[]> {
    return this.repository.findMany({ where: params.where });
  }
}
