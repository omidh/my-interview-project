import { ForbiddenException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Payment, User } from '@prisma/client';
import {
  paymentMockGenerator,
  userMockGenerator,
} from '../../test/mock-generators';
import { JwtPayload } from '../authentication/types/jwt-payload.type';
import { UsersService } from '../users/users.service';
import { PaymentsController } from './payments.controller';
import { PaymentsService } from './payments.service';

describe('PaymentsController', () => {
  let controller: PaymentsController;
  let service: PaymentsService;
  let usersService: UsersService;
  let user: User;
  let manager: User;
  let userPayload: JwtPayload;
  let managerPayload: JwtPayload;
  let payment: Payment;

  beforeEach(async () => {
    user = userMockGenerator();
    manager = userMockGenerator({ role: 'MANAGER' });
    userPayload = { username: user.username };
    managerPayload = { username: manager.username };
    payment = paymentMockGenerator({ username: user.username });
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaymentsController],
      providers: [
        {
          provide: PaymentsService,
          useValue: {
            create: jest.fn().mockResolvedValue(payment),
            findMany: jest.fn().mockResolvedValue([payment]),
          },
        },
        {
          provide: UsersService,
          useValue: {
            findOne: jest
              .fn()
              .mockImplementation((params) =>
                params.where.username === user.username
                  ? Promise.resolve(user)
                  : Promise.resolve(manager),
              ),
          },
        },
      ],
    }).compile();

    controller = module.get<PaymentsController>(PaymentsController);
    service = module.get<PaymentsService>(PaymentsService);
    usersService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it("should call service's create method and return it", async () => {
      await expect(
        controller.create({ planId: payment.planId }, userPayload),
      ).resolves.toEqual(payment);
      expect(service.create).toHaveBeenCalledWith({
        planId: payment.planId,
        username: userPayload.username,
      });
    });
  });

  describe('findMany', () => {
    it("should call service's findMany method and return its result", async () => {
      await expect(
        controller.findAll(userPayload, { username: user.username }),
      ).resolves.toEqual([payment]);
      expect(service.findMany).toHaveBeenCalledWith({
        where: { username: user.username },
      });
    });

    it('should fetch user', async () => {
      await controller.findAll(userPayload, { username: user.username });
      expect(usersService.findOne).toHaveBeenCalledWith({
        where: { username: user.username },
      });
    });

    it("should throw ForbiddenException if user is SIMPLE but wants to fetch another user's payments", async () => {
      const anotherUser = userMockGenerator();
      await expect(
        controller.findAll(userPayload, { username: user.username }),
      ).resolves.not.toThrow();
      await expect(
        controller.findAll(userPayload, { username: anotherUser.username }),
      ).rejects.toThrow(ForbiddenException);
    });

    it("should not throw ForbiddenException if user is MANAGER and wants to fetch another user's payments", async () => {
      await expect(
        controller.findAll(managerPayload, { username: user.username }),
      ).resolves.not.toThrow();
    });
  });
});
