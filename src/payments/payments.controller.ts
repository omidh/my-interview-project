import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  NotFoundException,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Payment } from '@prisma/client';
import { UserInRequest } from '../authentication/decorators/user-in-request.decorator';
import { JwtGuard } from '../authentication/guards/jwt.guard';
import { JwtPayload } from '../authentication/types/jwt-payload.type';
import { UsersService } from '../users/users.service';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { FindPaymentsDto } from './dto/find-payments.dto';
import { PaymentsService } from './payments.service';
import { PaymentsSchema } from './payments.swagger-schema';

@ApiTags('Payments')
@Controller('payments')
export class PaymentsController {
  constructor(
    private readonly paymentsService: PaymentsService,
    private readonly usersService: UsersService,
  ) {}

  @ApiBearerAuth()
  @ApiCreatedResponse({
    schema: PaymentsSchema,
    description: 'Payment has been created.',
  })
  @UseGuards(JwtGuard)
  @Post()
  create(
    @Body() body: CreatePaymentDto,
    @UserInRequest() userPayload: JwtPayload,
  ): Promise<Payment> {
    return this.paymentsService.create({
      planId: body.planId,
      username: userPayload.username,
    });
  }

  @ApiBearerAuth()
  @ApiOkResponse({
    schema: {
      type: 'array',
      uniqueItems: true,
      nullable: false,
      items: PaymentsSchema,
    },
    description: 'Payment has been created.',
  })
  @UseGuards(JwtGuard)
  @Get()
  async findAll(
    @UserInRequest() userPayload: JwtPayload,
    @Query() query: FindPaymentsDto,
  ): Promise<Payment[]> {
    const user = await this.usersService.findOne({
      where: { username: userPayload.username },
    });
    if (!user) {
      throw new NotFoundException('User not fonud!');
    }

    if (query.username !== user.username && user.role !== 'MANAGER') {
      throw new ForbiddenException(
        `You are not allowed to read payment of user ${query.username}`,
      );
    }

    return this.paymentsService.findMany({
      where: { username: userPayload.username },
    });
  }
}
