import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class FindPaymentsDto {
  @ApiProperty({ nullable: false, type: 'string' })
  @IsNotEmpty()
  @IsString()
  readonly username!: string;
}
