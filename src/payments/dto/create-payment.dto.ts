import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsPositive } from 'class-validator';

export class CreatePaymentDto {
  @ApiProperty({ nullable: false, type: 'integer', minimum: 1 })
  @IsNotEmpty()
  @IsPositive()
  @IsInt()
  readonly planId!: number;
}
