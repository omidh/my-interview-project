import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { PaymentStatus } from '@prisma/client';

export const PaymentsSchema: SchemaObject = {
  type: 'object',
  properties: {
    id: {
      type: 'string',
      nullable: false,
      minLength: 25,
    },
    username: {
      type: 'string',
      nullable: false,
    },
    url: {
      type: 'string',
      format: 'uri',
      nullable: false,
    },
    code: {
      type: 'integer',
      nullable: false,
      minimum: 1000,
      maximum: 9999,
    },
    planId: {
      type: 'integer',
      nullable: false,
    },
    status: {
      type: 'string',
      enum: Object.values(PaymentStatus),
      nullable: false,
    },
  },
  required: ['status', 'planId', 'code', 'url', 'username', 'id'],
};
