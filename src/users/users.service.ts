import { Injectable } from '@nestjs/common';
import { Prisma, User } from '@prisma/client';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class UsersService {
  private readonly repository: Prisma.UserDelegate<
    Prisma.RejectOnNotFound | Prisma.RejectPerOperation | undefined
  >;

  constructor(prismaService: PrismaService) {
    this.repository = prismaService.user;
  }

  findOne(params: {
    where: Prisma.UserWhereUniqueInput;
  }): Promise<User | null> {
    return this.repository.findUnique(params);
  }
}
