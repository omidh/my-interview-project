import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumberString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { GenerateVerificationCodeDto } from './generate-verification-code.dto';

export class LoginDto extends GenerateVerificationCodeDto {
  @ApiProperty({ type: 'string', maxLength: 6, minLength: 6, example: '123456' })
  @IsNotEmpty()
  @IsNumberString()
  @MaxLength(6)
  @MinLength(6)
  readonly verificationCode!: string;
}
