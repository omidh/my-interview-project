import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class GenerateVerificationCodeDto {
  @ApiProperty({ nullable: false })
  @IsNotEmpty()
  @IsString()
  readonly username!: string;

  @ApiProperty({ nullable: false })
  @IsNotEmpty()
  @IsString()
  readonly password!: string;
}
