import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '@prisma/client';
import { userMockGenerator } from '../../test/mock-generators';
import { UsersService } from '../users/users.service';
import { VerifierService } from '../verifier/verifier.service';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let usersService: UsersService;
  let jwtService: JwtService;
  let user: User;

  beforeEach(async () => {
    user = userMockGenerator();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthenticationService,
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn().mockResolvedValue(user),
          },
        },
        {
          provide: JwtService,
          useValue: {
            signAsync: jest.fn().mockResolvedValue('signedToken'),
          },
        },
      ],
    }).compile();

    service = module.get<AuthenticationService>(AuthenticationService);
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('authenticate', () => {
    let authenticateParams: Parameters<AuthenticationService['authenticate']>;
    beforeEach(() => {
      authenticateParams = [user.username, 'somePassword'];
    });

    it('should fetch user with its username', async () => {
      await service.authenticate(...authenticateParams);
      expect(usersService.findOne).toHaveBeenCalledWith({
        where: { username: user.username },
      });
    });

    it('should return null if user is not found', async () => {
      jest.spyOn(usersService, 'findOne').mockResolvedValue(null);
      await expect(
        service.authenticate(...authenticateParams),
      ).resolves.toBeNull();
    });

    it('should sign and return user payload', async () => {
      await expect(
        service.authenticate(...authenticateParams),
      ).resolves.toEqual('signedToken');
      expect(jwtService.signAsync).toHaveBeenCalledWith({
        username: user.username,
      }, { secret: 'someSecret' });
    });
  });
});
