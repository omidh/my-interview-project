import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  UnauthorizedException,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { VerifierService } from '../verifier/verifier.service';
import { AuthenticationService } from './authentication.service';
import { GenerateVerificationCodeDto } from './dto/generate-verification-code.dto';
import { LoginDto } from './dto/login.dto';

@ApiTags('Authentication')
@Controller('authentication')
export class AuthenticationController {
  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly verifierService: VerifierService,
  ) {}

  @ApiOkResponse({
    description: 'Verification code has been generated.',
    schema: {
      type: 'object',
      properties: {
        code: { type: 'string', nullable: false, minLength: 6, maxLength: 6 },
      },
      required: ['code'],
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid credentials or verification code.',
  })
  @Post('code')
  @HttpCode(HttpStatus.OK)
  async generateVerificationCode(
    @Body() body: GenerateVerificationCodeDto,
  ): Promise<{ code: string }> {
    const isCredentialsValid = await this.authenticationService.authenticate(
      body.username,
      body.password,
    );
    if (!isCredentialsValid) {
      throw new UnauthorizedException('Invalid username or password.');
    }

    const code = await this.verifierService.generateCode(body.username);
    return { code };
  }

  @ApiOkResponse({
    description: 'Token has been fetched.',
    schema: {
      type: 'object',
      properties: {
        token: {
          type: 'string',
          nullable: false,
          description: 'JWT signed token',
        },
      },
      required: ['token'],
    },
  })
  @ApiUnauthorizedResponse({
    description: 'Invalid credentials or verification code.',
  })
  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() body: LoginDto): Promise<{ token: string }> {
    const isCodeValid = await this.verifierService.verifyCode(
      body.username,
      body.verificationCode,
    );
    if (!isCodeValid) {
      throw new UnauthorizedException('Invalid verification code.');
    }

    const token = await this.authenticationService.authenticate(
      body.username,
      body.password,
    );
    if (!token) {
      throw new UnauthorizedException('Invalid username or password.');
    }

    return { token };
  }
}
