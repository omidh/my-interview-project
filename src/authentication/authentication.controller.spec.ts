import { UnauthorizedException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '@prisma/client';
import { userMockGenerator } from '../../test/mock-generators';
import { VerifierService } from '../verifier/verifier.service';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationController', () => {
  let controller: AuthenticationController;
  let service: AuthenticationService;
  let verifierService: VerifierService;
  let user: User;

  beforeEach(async () => {
    user = userMockGenerator();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthenticationController],
      providers: [
        {
          provide: AuthenticationService,
          useValue: {
            authenticate: jest.fn().mockResolvedValue('signedToken'),
          },
        },
        {
          provide: VerifierService,
          useValue: {
            verifyCode: jest.fn().mockResolvedValue(true),
            generateCode: jest.fn().mockResolvedValue('123456'),
          },
        },
      ],
    }).compile();

    controller = module.get<AuthenticationController>(AuthenticationController);
    service = module.get<AuthenticationService>(AuthenticationService);
    verifierService = module.get<VerifierService>(VerifierService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('generateVerificationCode', () => {
    it("should throw UnauthorizedException if service's authenticate method returns null", async () => {
      jest.spyOn(service, 'authenticate').mockResolvedValue(null);
      await expect(
        controller.generateVerificationCode({
          username: user.username,
          password: 'somePassword',
        }),
      ).rejects.toThrow(UnauthorizedException);
      expect(service.authenticate).toHaveBeenCalledWith(
        user.username,
        'somePassword',
      );
    });

    it("should call verifierService's generateCode method and return its result", async () => {
      await expect(
        controller.generateVerificationCode({
          username: user.username,
          password: 'somePassword',
        }),
      ).resolves.toEqual({ code: '123456' });
      expect(verifierService.generateCode).toHaveBeenCalledWith(user.username);
    });
  });

  describe('login', () => {
    it("should call service's authenticate method and return its result", async () => {
      await expect(
        controller.login({
          username: user.username,
          password: 'somePassword',
          verificationCode: '123456',
        }),
      ).resolves.toEqual({ token: 'signedToken' });
      expect(service.authenticate).toHaveBeenCalledWith(
        user.username,
        'somePassword',
      );
    });

    it("should throw UnauthorizedException if service's authenticate method returns null", async () => {
      jest.spyOn(service, 'authenticate').mockResolvedValue(null);
      await expect(
        controller.login({
          username: user.username,
          password: 'somePassword',
          verificationCode: '123456',
        }),
      ).rejects.toThrow(UnauthorizedException);
    });

    it("should throw UnauthorizedException if verifierService's verifyCode method returns null", async () => {
      jest.spyOn(verifierService, 'verifyCode').mockResolvedValue(false);
      await expect(
        controller.login({
          username: user.username,
          password: 'somePassword',
          verificationCode: '123456',
        }),
      ).rejects.toThrow(UnauthorizedException);
      expect(verifierService.verifyCode).toHaveBeenCalledWith(
        user.username,
        '123456',
      );
    });
  });
});
