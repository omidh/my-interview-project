import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) { }

  /**
   * Verifies username and password credentials
   *
   * Checking passport is omitted since this is just for interview purpose
   *
   * @param {string} username
   * @param {string} password
   * @returns `string` jwt token if credentials were valid
   * @returns `null` credentials were invalid
   */
  async authenticate(
    username: string,
    password: string,
  ): Promise<string | null> {
    const user = await this.usersService.findOne({ where: { username } });
    if (!user) {
      return null;
    }

    return this.jwtService.signAsync({ username: user.username }, { secret: 'someSecret' });
  }
}
